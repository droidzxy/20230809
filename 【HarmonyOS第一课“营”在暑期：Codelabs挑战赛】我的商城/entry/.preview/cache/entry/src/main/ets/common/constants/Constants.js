/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// thousandth
export const GOODS_LIST_HEIGHT = '20%';
export const GOODS_IMAGE_WIDTH = '40%';
export const GOODS_FONT_WIDTH = '60%';
export const GOODS_LIST_WIDTH = '94%';
export const LAYOUT_WIDTH_OR_HEIGHT = '100%';
// font-size
export const GOODS_LIST_PADDING = 8;
export const GOODS_EVALUATE_FONT_SIZE = 12;
export const NORMAL_FONT_SIZE = 16;
export const BIGGER_FONT_SIZE = 20;
export const MAX_FONT_SIZE = 32;
// margin
export const REFRESH_ICON_MARGIN_RIGHT = 20;
export const MARGIN_RIGHT = 32;
// width or height
export const ICON_WIDTH = 40;
export const ICON_HEIGHT = 40;
// list space
export const LIST_ITEM_SPACE = 16;
// navigation title
export const STORE = '商城';
// offset range
export const MAX_OFFSET_Y = 100;
// refresh time
export const REFRESH_TIME = 1500;
// Magnification
export const MAGNIFICATION = 2;
export const MAX_DATA_LENGTH = 12;
/**
 * The font weight of application.
 */
var AppFontWeight;
(function (AppFontWeight) {
    AppFontWeight["BOLD"] = "400";
    AppFontWeight["BOLDER"] = "500";
})(AppFontWeight || (AppFontWeight = {}));
;
/**
 * The font size of application.
 */
var AppFontSize;
(function (AppFontSize) {
    AppFontSize[AppFontSize["SMALLER"] = 13] = "SMALLER";
    AppFontSize[AppFontSize["SMALL"] = 14] = "SMALL";
    AppFontSize[AppFontSize["MIDDLE"] = 16] = "MIDDLE";
    AppFontSize[AppFontSize["LARGE"] = 20] = "LARGE";
    AppFontSize[AppFontSize["LARGER"] = 35] = "LARGER";
})(AppFontSize || (AppFontSize = {}));
;
export { AppFontSize, AppFontWeight };
/**
 * The percentage of 100.
 */
export const PERCENTAGE_100 = '100%';
/**
 * The percentage of 20.
 */
export const PERCENTAGE_20 = '20%';
/**
 * The percentage of 50.
 */
export const PERCENTAGE_50 = '50%';
/**
 * The text margin of 18.
 */
export const Text_Margin = 18;
/**
 * The style of CardComponent.
 */
export const CardStyle = {
    /**
     * The horizontal margin of CardComponent.
     */
    CARD_MARGIN_HORIZONTAL: 16,
    /**
     * The top margin of CardComponent.
     */
    CARD_MARGIN_TOP: 24,
    /**
     * The vertical padding of CardComponent.
     */
    CARD_PADDING_VERTICAL: 25,
    /**
     * The horizontal padding of CardComponent.
     */
    CARD_PADDING_HORIZONTAL: 16,
    /**
     * The border radius of CardComponent.
     */
    CARD_RADIUS: 18,
};
/**
 * The style of BalanceComponent.
 */
export const BalanceStyle = {
    /**
     * The layout weight of BalanceComponent.
     */
    LAYOUT_WEIGHT: 1,
};
/**
 * The style of ToolBarComponent.
 */
export const ToolBarStyle = {
    /**
     * The bottom margin.
     */
    MARGIN_BOTTOM: 10,
    /**
     * The size of image.
     */
    IMAGE_SIZE: 24,
    /**
     * The weight of layout.
     */
    LAYOUT_WEIGHT: '25%',
    /**
     * The height of divider.
     */
    DIVIDER_HEIGHT: 0.5,
    /**
     * The opacity of divider.
     */
    DIVIDER_OPACITY: 0.05
};
/**
 * The style of NavPage.
 */
export const NavPageStyle = {
    /**
     * The height of Tabs.
     */
    BAR_HEIGHT: 0,
    /**
     * The default position of Tabs.
     */
    POSITION_INITIAL: 0,
};
/**
 * The style of HomePage.
 */
export const HomePageStyle = {
    /**
     * The vertical padding.
     */
    PADDING_VERTICAL: 8,
    /**
     * The horizontal padding.
     */
    PADDING_HORIZONTAL: 16,
    /**
     * The height of blank.
     */
    BLANK_HEIGHT: 15,
};
/**
 * The style of DetailsPage.
 */
export const DetailsPageStyle = {
    /**
     * The size of top image.
     */
    TOP_IMAGE_SIZE: 32,
    /**
     * The margin of top.
     */
    MARGIN_TOP_LAYOUT: 16,
    /**
     * The top margin of title.
     */
    TITLE_MARGIN_TOP: 4,
    /**
     * The bottom margin of title.
     */
    TITLE_MARGIN_BOTTOM: 20,
    /**
     * The height of divider.
     */
    DIVIDER_HEIGHT: 0.25,
    /**
     * The left margin of divider.
     */
    DIVIDER_MARGIN_LEFT: '12%',
    /**
     * The top margin of divider.
     */
    DIVIDER_MARGIN_TOP: 10,
    /**
     * The bottom margin of divider.
     */
    DIVIDER_MARGIN_BOTTOM: 18,
    /**
     * The top margin of comment list.
     */
    COMMENT_LIST_MARGIN_TOP: 10,
    /**
     * The height of top layout.
     */
    TOP_LAYOUT_HEIGHT: '45%',
    /**
     * The height of scroll layout.
     */
    SCROLL_LAYOUT_WEIGHT: '93%',
    /**
     * The weight of tool bar layout.
     */
    TOOLBAR_WEIGHT: '7%',
    /**
     * The image size for more action.
     */
    IMAGE_SIZE_MORE: 14
};
/**
 * ability name.
 */
export const DETAILS_ABILITY_NAME = 'DetailsAbility';
//# sourceMappingURL=Constants.js.map