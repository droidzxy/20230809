/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export const initTabBarData = [
    { "id": 16777287, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
    { "id": 16777226, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
    { "id": 16777313, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
    { "id": 16777264, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" }
];
export const goodsInitialList = [
    {
        goodsImg: { "id": 16777317, "type": 20000, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        goodsName: { "id": 16777299, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        advertising_language: { "id": 16777297, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        evaluate: { "id": 16777298, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        price: { "id": 16777300, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        position: 0
    },
    {
        goodsImg: { "id": 16777220, "type": 20000, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        goodsName: { "id": 16777285, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        advertising_language: { "id": 16777283, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        evaluate: { "id": 16777284, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        price: { "id": 16777286, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        position: 1
    },
    {
        goodsImg: { "id": 16777316, "type": 20000, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        goodsName: { "id": 16777295, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        advertising_language: { "id": 16777293, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        evaluate: { "id": 16777294, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        price: { "id": 16777296, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        position: 2
    },
    {
        goodsImg: { "id": 16777314, "type": 20000, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        goodsName: { "id": 16777311, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        advertising_language: { "id": 16777309, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        evaluate: { "id": 16777310, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        price: { "id": 16777312, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        position: 3
    },
];
//# sourceMappingURL=InitialData.js.map