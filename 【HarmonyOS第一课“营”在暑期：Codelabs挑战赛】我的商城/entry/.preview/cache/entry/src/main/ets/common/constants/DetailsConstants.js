/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * The constant of GoodsTitleComponent.
 */
export const GoodsTitle = {
    /**
     * The max lines of title text.
     */
    MAX_LINE: 2,
    /**
     * The line height of title text.
     */
    LINE_HEIGHT: 32,
};
/**
 * The constant of PanelComponent.
 */
export const ActionPanel = {
    /**
     * The image height of PanelComponent.
     */
    IMAGE_HEIGHT: 18,
    /**
     * The blank width of PanelComponent.
     */
    BLANK_WIDTH: 4,
};
/**
 * The constant of AddressComponent.
 */
export const AddressPicker = {
    /**
     * The size of image to show loaction.
     */
    IMAGE_SIZE_LOCATION: 18,
    /**
     * The image size for more operations.
     */
    IMAGE_SIZE_MORE: 14,
    /**
     * The max lines.
     */
    MAX_LINES: 1,
    /**
     * The layout weight of left.
     */
    LAYOUT_WEIGHT_LEFT: '12%',
    /**
     * The layout weight of center.
     */
    LAYOUT_WEIGHT_CENTER: '80%',
    /**
     * The layout weight of right.
     */
    LAYOUT_WEIGHT_RIGHT: '8%',
    /**
     * The margin of right image.
     */
    MARGIN_RIGHT_IMAGE: '2%',
};
/**
 * The constant of CommentsComponent.
 */
export const CommentList = {
    /**
     * The space of List.
     */
    SPACE: 5,
    /**
     * The image size of head portrait.
     */
    IMAGE_SIZE_HEAD_PORTRAIT: 45,
    /**
     * The margin of head portrait.
     */
    MARGIN_HEAD_PORTRAIT: 16,
    /**
     * The line height of phone number text.
     */
    LINE_HEIGHT_PHONE: 32,
    /**
     * The blank height of phone number text.
     */
    BLANK_HEIGHT_PHONE: 6,
    /**
     * The width of blank.
     */
    MARK_BLANK_WIDTH: 15,
    /**
     * The width of image.
     */
    IMAGE_MARK_WIDTH: 20,
    /**
     * The max lines.
     */
    COMMENT_MAX_LINES: 1,
    /**
     * The line height.
     */
    COMMENT_LINE_HEIGHT: 26,
    /**
     * The margin of text.
     */
    MARGIN_TEXT: 5,
    /**
     * The height of List.
     */
    HEIGHT: '40%',
};
/**
 * The constant of CommentsHeaderComponent.
 */
export const CommentHeader = {
    /**
     * The layout weight.
     */
    LAYOUT_WEIGHT: '50%',
    /**
     * The arrow text.
     */
    ARROW: '  >',
    /**
     * The line height.
     */
    LINE_HEIGHT: 20,
};
/**
 * The constant of BottomBarComponent.
 */
export const BottomBar = {
    /**
     * The height of divider.
     */
    DIVIDER_HEIGHT: 0.5,
    /**
     * The line height of text.
     */
    TEXT_LINE_HEIGHT: 20,
    /**
     * The width of container.
     */
    CONTAINER_SIZE_WIDTH: 195,
    /**
     * The height of container.
     */
    CONTAINER_SIZE_HEIGHT: 40,
    /**
     * The padding of right.
     */
    BAR_PADDING_RIGHT: 12,
    /**
     * The padding of top.
     */
    BAR_PADDING_TOP: 9,
    /**
     * The padding of bottom.
     */
    BAR_PADDING_BOTTOM: 7,
    /**
     * The width of text.
     */
    TEXT_WIDTH: '50%',
};
/**
 * The constant of PickerComponent.
 */
export const GoodsPicker = {
    /**
     * The line height of GoodsPicker text.
     */
    LINE_HEIGHT_TEXT_SELECTED: 25,
    /**
     * The max lines.
     */
    MAX_LINES: 2,
    /**
     * The lines height of description text.
     */
    LINE_HEIGHT_DESCRIPTION: 25,
    /**
     * The left margin of description text.
     */
    MARGIN_LEFT_DESCRIPTION: 15,
    /**
     * The left weight of layout.
     */
    LAYOUT_WEIGHT_LEFT: '10%',
    /**
     * The center weight of layout.
     */
    LAYOUT_WEIGHT_CENTER: '80%',
    /**
     * The right weight of layout.
     */
    LAYOUT_WEIGHT_RIGHT: '10%',
};
/**
 * The constant of PreviewerComponent.
 */
export const GoodsPreviewer = {
    /**
     * The bottom padding of image.
     */
    PADDING_IMAGE_BOTTOM: 40,
    /**
     * The top padding of image.
     */
    PADDING_IMAGE_TOP: 20,
    /**
     * The text of indicator.
     */
    INDICATOR_TEXT: '1/4',
    /**
     * The right margin of indicator.
     */
    INDICATOR_MARGIN_RIGHT: 24,
    /**
     * The bottom margin of indicator.
     */
    INDICATOR_MARGIN_BOTTOM: 17,
    /**
     * The horizontal padding of indicator.
     */
    INDICATOR_PADDING_HORIZONTAL: 10,
    /**
     * The top padding of indicator.
     */
    INDICATOR_PADDING_TOP: 3,
    /**
     * The bottom padding of indicator.
     */
    INDICATOR_PADDING_BOTTOM: 2,
    /**
     * The border radius.
     */
    BORDER_RADIUS: 9,
    /**
     * The location text.
     */
    LOCATION: "86%",
    /**
     * The zIndex of layout.
     */
    INDEX: 2,
};
/**
 * The constant of ServiceItemComponent.
 */
export const GoodsServiceItem = {
    /**
     * The max lines of text.
     */
    MAX_LINES: 1,
    /**
     * The size of service image.
     */
    IMAGE_SIZE_SERVICE: 18,
    /**
     * The size of more action image.
     */
    IMAGE_SIZE_MORE: 14,
    /**
     * The layout weight of left.
     */
    LAYOUT_WEIGHT_LEFT: '12%',
    /**
     * The layout weight of center.
     */
    LAYOUT_WEIGHT_CENTER: '80%',
    /**
     * The layout weight of right.
     */
    LAYOUT_WEIGHT_RIGHT: '8%',
    /**
     * The right margin of image.
     */
    MARGIN_RIGHT_IMAGE: '2%',
};
/**
 * The constant of ServicesComponent.
 */
export const GoodsService = {
    /**
     * The space of List.
     */
    SPACE: 20,
    /**
     * The height of List.
     */
    LIST_HEIGHT: 94,
};
//# sourceMappingURL=DetailsConstants.js.map