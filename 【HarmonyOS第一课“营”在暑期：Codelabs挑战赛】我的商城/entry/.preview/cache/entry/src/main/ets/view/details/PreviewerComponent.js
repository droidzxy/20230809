/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AppFontSize, PERCENTAGE_100 } from '@bundle:com.example.list_harmony/entry/ets/common/constants/Constants';
import { GoodsPreviewer } from '@bundle:com.example.list_harmony/entry/ets/common/constants/DetailsConstants';
export default class PreviewerComponent extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.goodsImg = undefined;
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.goodsImg !== undefined) {
            this.goodsImg = params.goodsImg;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("view/details/PreviewerComponent.ets(24:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/PreviewerComponent.ets(25:7)");
            Row.width(PERCENTAGE_100);
            Row.height(PERCENTAGE_100);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create(this.goodsImg);
            Image.debugLine("view/details/PreviewerComponent.ets(26:9)");
            Image.height(PERCENTAGE_100);
            Image.width(PERCENTAGE_100);
            Image.objectFit(ImageFit.Contain);
            Image.padding({ top: GoodsPreviewer.PADDING_IMAGE_TOP,
                bottom: GoodsPreviewer.PADDING_IMAGE_BOTTOM });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/PreviewerComponent.ets(36:7)");
            Row.zIndex(GoodsPreviewer.INDEX);
            Row.alignSelf(ItemAlign.End);
            Row.position({ x: GoodsPreviewer.LOCATION,
                y: GoodsPreviewer.LOCATION });
            Row.backgroundColor({ "id": 16777326, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Row.borderRadius(GoodsPreviewer.BORDER_RADIUS);
            Row.padding({
                top: GoodsPreviewer.INDICATOR_PADDING_TOP,
                bottom: GoodsPreviewer.INDICATOR_PADDING_BOTTOM,
                right: GoodsPreviewer.INDICATOR_PADDING_HORIZONTAL,
                left: GoodsPreviewer.INDICATOR_PADDING_HORIZONTAL
            });
            Row.margin({ bottom: GoodsPreviewer.INDICATOR_MARGIN_BOTTOM,
                right: GoodsPreviewer.INDICATOR_MARGIN_RIGHT });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(GoodsPreviewer.INDICATOR_TEXT);
            Text.debugLine("view/details/PreviewerComponent.ets(37:9)");
            Text.fontColor(Color.White);
            Text.fontSize(AppFontSize.SMALLER);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=PreviewerComponent.js.map