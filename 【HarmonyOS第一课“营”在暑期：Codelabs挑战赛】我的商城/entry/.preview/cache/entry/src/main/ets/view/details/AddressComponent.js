/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AppFontSize, AppFontWeight, PERCENTAGE_100 } from '@bundle:com.example.list_harmony/entry/ets/common/constants/Constants';
import { AddressPicker } from '@bundle:com.example.list_harmony/entry/ets/common/constants/DetailsConstants';
export default class AddressComponent extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/AddressComponent.ets(22:5)");
            Row.width(PERCENTAGE_100);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("view/details/AddressComponent.ets(23:7)");
            Column.width(AddressPicker.LAYOUT_WEIGHT_LEFT);
            Column.alignItems(HorizontalAlign.Start);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 16777229, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Text.debugLine("view/details/AddressComponent.ets(24:9)");
            Text.maxLines(AddressPicker.MAX_LINES);
            Text.textOverflow({ overflow: TextOverflow.Ellipsis });
            Text.textCase(TextCase.UpperCase);
            Text.fontSize(AppFontSize.SMALL);
            Text.fontColor({ "id": 16777336, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Text.fontWeight(AppFontWeight.BOLDER);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/AddressComponent.ets(35:7)");
            Row.justifyContent(FlexAlign.Start);
            Row.width(AddressPicker.LAYOUT_WEIGHT_CENTER);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 0, "type": 30000, params: ['detail/detail_location.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Image.debugLine("view/details/AddressComponent.ets(36:9)");
            Image.objectFit(ImageFit.Contain);
            Image.height(AddressPicker.IMAGE_SIZE_LOCATION);
            Image.width(AddressPicker.IMAGE_SIZE_LOCATION);
            Image.margin({ right: AddressPicker.MARGIN_RIGHT_IMAGE });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 16777228, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Text.debugLine("view/details/AddressComponent.ets(41:9)");
            Text.maxLines(AddressPicker.MAX_LINES);
            Text.textOverflow({ overflow: TextOverflow.Ellipsis });
            Text.textCase(TextCase.UpperCase);
            Text.fontSize(AppFontSize.SMALL);
            Text.fontColor(Color.Black);
            Text.fontWeight(AppFontWeight.BOLD);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/AddressComponent.ets(52:7)");
            Row.width(AddressPicker.LAYOUT_WEIGHT_RIGHT);
            Row.justifyContent(FlexAlign.End);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 0, "type": 30000, params: ['detail/detail_pick_up_more.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Image.debugLine("view/details/AddressComponent.ets(53:9)");
            Image.objectFit(ImageFit.Contain);
            Image.height(AddressPicker.IMAGE_SIZE_MORE);
            Image.width(AddressPicker.IMAGE_SIZE_MORE);
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Row.pop();
        Row.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=AddressComponent.js.map