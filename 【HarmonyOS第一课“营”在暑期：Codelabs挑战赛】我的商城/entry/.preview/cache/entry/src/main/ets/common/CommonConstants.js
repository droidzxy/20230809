/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// thousandth
export const GOODS_LIST_HEIGHT = '20%';
export const GOODS_IMAGE_WIDTH = '40%';
export const GOODS_FONT_WIDTH = '60%';
export const GOODS_LIST_WIDTH = '94%';
export const LAYOUT_WIDTH_OR_HEIGHT = '100%';
// font-size
export const GOODS_LIST_PADDING = 8;
export const GOODS_EVALUATE_FONT_SIZE = 12;
export const NORMAL_FONT_SIZE = 16;
export const BIGGER_FONT_SIZE = 20;
export const MAX_FONT_SIZE = 32;
// margin
export const REFRESH_ICON_MARGIN_RIGHT = 20;
export const MARGIN_RIGHT = 32;
// width or height
export const ICON_WIDTH = 40;
export const ICON_HEIGHT = 40;
// list space
export const LIST_ITEM_SPACE = 16;
// navigation title
export const STORE = '商城';
// offset range
export const MAX_OFFSET_Y = 100;
// refresh time
export const REFRESH_TIME = 1500;
// Magnification
export const MAGNIFICATION = 2;
export const MAX_DATA_LENGTH = 12;
//# sourceMappingURL=CommonConstants.js.map