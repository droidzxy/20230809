import AddressComponent from '@bundle:com.example.list_harmony/entry/ets/view/details/AddressComponent';
import PickerComponent from '@bundle:com.example.list_harmony/entry/ets/view/details/PickerComponent';
import PriceComponent from '@bundle:com.example.list_harmony/entry/ets/view/details/PriceComponent';
import GoodsTitleComponent from '@bundle:com.example.list_harmony/entry/ets/view/details/GoodsTitleComponent';
import PanelComponent from '@bundle:com.example.list_harmony/entry/ets/view/details/PanelComponent';
import CommentsComponent from '@bundle:com.example.list_harmony/entry/ets/view/details/CommentsComponent';
import CommentsHeaderComponent from '@bundle:com.example.list_harmony/entry/ets/view/details/CommentsHeaderComponent';
import BottomBarComponent from '@bundle:com.example.list_harmony/entry/ets/view/details/BottomBarComponent';
import ServicesComponent from '@bundle:com.example.list_harmony/entry/ets/view/details/ServicesComponent';
import PreviewerComponent from '@bundle:com.example.list_harmony/entry/ets/view/details/PreviewerComponent';
import DetailsViewModel from '@bundle:com.example.list_harmony/entry/ets/viewmodel/DetailsViewModel';
import { DetailsPageStyle, PERCENTAGE_100 } from '@bundle:com.example.list_harmony/entry/ets/common/constants/Constants';
import BalanceComponent from '@bundle:com.example.list_harmony/entry/ets/view/BalanceComponent';
import CardComponent from '@bundle:com.example.list_harmony/entry/ets/view/CardComponent';
let viewModel = new DetailsViewModel();
const KEY = 'GoodsPosition';
let position = AppStorage.Get(KEY);
function __Image__setTopImageStyle() {
    Image.width(DetailsPageStyle.TOP_IMAGE_SIZE);
    Image.height(DetailsPageStyle.TOP_IMAGE_SIZE);
}
class DetailsPage extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.goodsDetails = undefined;
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.goodsDetails !== undefined) {
            this.goodsDetails = params.goodsDetails;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    aboutToAppear() {
        this.goodsDetails = viewModel.loadDetails(position);
    }
    onBackPress() {
        let handler = getContext(this);
        handler.terminateSelf();
        return true;
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/DetailsPage.ets(60:5)");
            Column.height(PERCENTAGE_100);
            Column.width(PERCENTAGE_100);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Scroll.create();
            Scroll.debugLine("pages/DetailsPage.ets(61:7)");
            Scroll.height(DetailsPageStyle.SCROLL_LAYOUT_WEIGHT);
            Scroll.backgroundColor({ "id": 16777318, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            if (!isInitialRender) {
                Scroll.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/DetailsPage.ets(62:9)");
            Column.width(PERCENTAGE_100);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Stack.create({ alignContent: Alignment.Top });
            Stack.debugLine("pages/DetailsPage.ets(63:11)");
            Stack.height(DetailsPageStyle.TOP_LAYOUT_HEIGHT);
            Stack.width(PERCENTAGE_100);
            Stack.backgroundColor({ "id": 16777319, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            if (!isInitialRender) {
                Stack.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new 
                    // GoodsPreviewer displays images about goods.
                    PreviewerComponent(this, { goodsImg: this.goodsDetails.goodsImg }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        this.TopBarLayout.bind(this)();
        Stack.pop();
        // the card layout style about goods information.
        this.CardsLayout.bind(this)();
        Column.pop();
        Scroll.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            __Common__.create();
            __Common__.height(DetailsPageStyle.TOOLBAR_WEIGHT);
            if (!isInitialRender) {
                __Common__.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new 
                    // tool bar in the bottom.
                    BottomBarComponent(this, {}, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        __Common__.pop();
        Column.pop();
    }
    BackLayout(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 0, "type": 30000, params: ['detail/detail_back.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Image.debugLine("pages/DetailsPage.ets(85:5)");
            __Image__setTopImageStyle();
            Image.onClick(() => {
                let handler = getContext(this);
                handler.terminateSelf();
            });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
    }
    ShareLayout(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 0, "type": 30000, params: ['detail/detail_share.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Image.debugLine("pages/DetailsPage.ets(94:5)");
            __Image__setTopImageStyle();
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
    }
    TopBarLayout(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            __Common__.create();
            __Common__.width(PERCENTAGE_100);
            __Common__.padding(DetailsPageStyle.MARGIN_TOP_LAYOUT);
            if (!isInitialRender) {
                __Common__.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new BalanceComponent(this, {
                        left: () => {
                            this.BackLayout();
                        }, right: () => {
                            this.ShareLayout();
                        }
                    }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        __Common__.pop();
    }
    MoreActionBuilder(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 0, "type": 30000, params: ['detail/detail_pick_up_more.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Image.debugLine("pages/DetailsPage.ets(104:5)");
            Image.objectFit(ImageFit.Contain);
            Image.height(DetailsPageStyle.IMAGE_SIZE_MORE);
            Image.width(DetailsPageStyle.IMAGE_SIZE_MORE);
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
    }
    CardsLayout(parent = null) {
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new CardComponent(this, {
                        children: () => {
                            {
                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    if (isInitialRender) {
                                        ViewPU.create(new PriceComponent(this, { price: this.goodsDetails.price }, undefined, elmtId));
                                    }
                                    else {
                                        this.updateStateVarsOfChildByElmtId(elmtId, {});
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                });
                            }
                            this.observeComponentCreation((elmtId, isInitialRender) => {
                                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                __Common__.create();
                                __Common__.margin({ top: DetailsPageStyle.TITLE_MARGIN_TOP,
                                    bottom: DetailsPageStyle.TITLE_MARGIN_BOTTOM });
                                if (!isInitialRender) {
                                    __Common__.pop();
                                }
                                ViewStackProcessor.StopGetAccessRecording();
                            });
                            {
                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    if (isInitialRender) {
                                        ViewPU.create(new GoodsTitleComponent(this, { title: this.goodsDetails.goodsName }, undefined, elmtId));
                                    }
                                    else {
                                        this.updateStateVarsOfChildByElmtId(elmtId, {});
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                });
                            }
                            __Common__.pop();
                            {
                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    if (isInitialRender) {
                                        ViewPU.create(new PanelComponent(this, {}, undefined, elmtId));
                                    }
                                    else {
                                        this.updateStateVarsOfChildByElmtId(elmtId, {});
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                });
                            }
                        }
                    }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new CardComponent(this, {
                        children: () => {
                            {
                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    if (isInitialRender) {
                                        ViewPU.create(new PickerComponent(this, { goodsDescription: this.goodsDetails.advertising_language,
                                            actionMoreBuilder: this.MoreActionBuilder }, undefined, elmtId));
                                    }
                                    else {
                                        this.updateStateVarsOfChildByElmtId(elmtId, {});
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                });
                            }
                        }
                    }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new CardComponent(this, {
                        children: () => {
                            {
                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    if (isInitialRender) {
                                        ViewPU.create(new AddressComponent(this, {}, undefined, elmtId));
                                    }
                                    else {
                                        this.updateStateVarsOfChildByElmtId(elmtId, {});
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                });
                            }
                            this.observeComponentCreation((elmtId, isInitialRender) => {
                                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                Divider.create();
                                Divider.debugLine("pages/DetailsPage.ets(126:7)");
                                Divider.height(DetailsPageStyle.DIVIDER_HEIGHT);
                                Divider.backgroundColor({ "id": 16777325, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
                                Divider.margin({
                                    left: DetailsPageStyle.DIVIDER_MARGIN_LEFT,
                                    bottom: DetailsPageStyle.DIVIDER_MARGIN_BOTTOM,
                                    top: DetailsPageStyle.DIVIDER_MARGIN_TOP
                                });
                                if (!isInitialRender) {
                                    Divider.pop();
                                }
                                ViewStackProcessor.StopGetAccessRecording();
                            });
                            {
                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    if (isInitialRender) {
                                        ViewPU.create(new ServicesComponent(this, {}, undefined, elmtId));
                                    }
                                    else {
                                        this.updateStateVarsOfChildByElmtId(elmtId, {});
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                });
                            }
                        }
                    }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new CardComponent(this, {
                        children: () => {
                            {
                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    if (isInitialRender) {
                                        ViewPU.create(new CommentsHeaderComponent(this, {}, undefined, elmtId));
                                    }
                                    else {
                                        this.updateStateVarsOfChildByElmtId(elmtId, {});
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                });
                            }
                            this.observeComponentCreation((elmtId, isInitialRender) => {
                                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                __Common__.create();
                                __Common__.margin({ top: DetailsPageStyle.COMMENT_LIST_MARGIN_TOP });
                                if (!isInitialRender) {
                                    __Common__.pop();
                                }
                                ViewStackProcessor.StopGetAccessRecording();
                            });
                            {
                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    if (isInitialRender) {
                                        ViewPU.create(new CommentsComponent(this, {}, undefined, elmtId));
                                    }
                                    else {
                                        this.updateStateVarsOfChildByElmtId(elmtId, {});
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                });
                            }
                            __Common__.pop();
                        }
                    }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new DetailsPage(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=DetailsPage.js.map