/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AppFontSize, AppFontWeight, PERCENTAGE_100 } from '@bundle:com.example.list_harmony/entry/ets/common/constants/Constants';
import { GoodsServiceItem } from '@bundle:com.example.list_harmony/entry/ets/common/constants/DetailsConstants';
export default class ServiceItemComponent extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.serviceName = undefined;
        this.description = undefined;
        this.isShowActionMore = undefined;
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.serviceName !== undefined) {
            this.serviceName = params.serviceName;
        }
        if (params.description !== undefined) {
            this.description = params.description;
        }
        if (params.isShowActionMore !== undefined) {
            this.isShowActionMore = params.isShowActionMore;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/ServiceItemComponent.ets(26:5)");
            Row.width(PERCENTAGE_100);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("view/details/ServiceItemComponent.ets(27:7)");
            Column.width(GoodsServiceItem.LAYOUT_WEIGHT_LEFT);
            Column.alignItems(HorizontalAlign.Start);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            var _a;
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create((_a = this.serviceName) !== null && _a !== void 0 ? _a : '');
            Text.debugLine("view/details/ServiceItemComponent.ets(28:9)");
            Text.maxLines(GoodsServiceItem.MAX_LINES);
            Text.textOverflow({ overflow: TextOverflow.Ellipsis });
            Text.textCase(TextCase.UpperCase);
            Text.fontSize(AppFontSize.SMALL);
            Text.fontColor({ "id": 16777336, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Text.fontWeight(AppFontWeight.BOLDER);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/ServiceItemComponent.ets(40:7)");
            Row.justifyContent(FlexAlign.Start);
            Row.width(GoodsServiceItem.LAYOUT_WEIGHT_CENTER);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 0, "type": 30000, params: ['detail/detail_services.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Image.debugLine("view/details/ServiceItemComponent.ets(41:9)");
            Image.objectFit(ImageFit.Contain);
            Image.height(GoodsServiceItem.IMAGE_SIZE_SERVICE);
            Image.width(GoodsServiceItem.IMAGE_SIZE_SERVICE);
            Image.margin({ right: GoodsServiceItem.MARGIN_RIGHT_IMAGE });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            var _a;
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create((_a = this.description) !== null && _a !== void 0 ? _a : '');
            Text.debugLine("view/details/ServiceItemComponent.ets(46:9)");
            Text.maxLines(GoodsServiceItem.MAX_LINES);
            Text.textOverflow({ overflow: TextOverflow.Ellipsis });
            Text.textCase(TextCase.UpperCase);
            Text.fontSize(AppFontSize.SMALL);
            Text.fontColor({ "id": 16777336, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Text.fontWeight(AppFontWeight.BOLDER);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/ServiceItemComponent.ets(58:7)");
            Row.width(GoodsServiceItem.LAYOUT_WEIGHT_RIGHT);
            Row.justifyContent(FlexAlign.End);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (this.isShowActionMore) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Image.create({ "id": 0, "type": 30000, params: ['detail/detail_pick_up_more.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
                        Image.debugLine("view/details/ServiceItemComponent.ets(60:11)");
                        Image.objectFit(ImageFit.Contain);
                        Image.height(GoodsServiceItem.IMAGE_SIZE_MORE);
                        Image.width(GoodsServiceItem.IMAGE_SIZE_MORE);
                        if (!isInitialRender) {
                            Image.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                });
            }
            else {
                this.ifElseBranchUpdateFunction(1, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Row.create();
                        Row.debugLine("view/details/ServiceItemComponent.ets(65:11)");
                        Row.height(GoodsServiceItem.IMAGE_SIZE_MORE);
                        Row.width(GoodsServiceItem.IMAGE_SIZE_MORE);
                        if (!isInitialRender) {
                            Row.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Row.pop();
                });
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        Row.pop();
        Row.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=ServiceItemComponent.js.map