/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AppFontSize, AppFontWeight, PERCENTAGE_100 } from '@bundle:com.example.list_harmony/entry/ets/common/constants/Constants';
import { GoodsPicker } from '@bundle:com.example.list_harmony/entry/ets/common/constants/DetailsConstants';
export default class PickerComponent extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.goodsDescription = undefined;
        this.actionMoreBuilder = undefined;
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.goodsDescription !== undefined) {
            this.goodsDescription = params.goodsDescription;
        }
        if (params.actionMoreBuilder !== undefined) {
            this.actionMoreBuilder = params.actionMoreBuilder;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/PickerComponent.ets(25:5)");
            Row.width(PERCENTAGE_100);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 16777236, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Text.debugLine("view/details/PickerComponent.ets(26:7)");
            Text.maxLines(GoodsPicker.MAX_LINES);
            Text.lineHeight(GoodsPicker.LINE_HEIGHT_TEXT_SELECTED);
            Text.textOverflow({ overflow: TextOverflow.Ellipsis });
            Text.textCase(TextCase.UpperCase);
            Text.fontSize(AppFontSize.SMALL);
            Text.fontColor(Color.Black);
            Text.fontWeight(AppFontWeight.BOLDER);
            Text.width(GoodsPicker.LAYOUT_WEIGHT_LEFT);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/PickerComponent.ets(36:7)");
            Row.width(GoodsPicker.LAYOUT_WEIGHT_CENTER);
            Row.justifyContent(FlexAlign.Start);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(this.goodsDescription);
            Text.debugLine("view/details/PickerComponent.ets(37:9)");
            Text.maxLines(GoodsPicker.MAX_LINES);
            Text.lineHeight(GoodsPicker.LINE_HEIGHT_DESCRIPTION);
            Text.textOverflow({ overflow: TextOverflow.Ellipsis });
            Text.textCase(TextCase.UpperCase);
            Text.fontSize(AppFontSize.SMALL);
            Text.fontColor({ "id": 16777336, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Text.fontWeight(AppFontWeight.BOLD);
            Text.padding({ left: GoodsPicker.MARGIN_LEFT_DESCRIPTION });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/PickerComponent.ets(50:7)");
            Row.width(GoodsPicker.LAYOUT_WEIGHT_RIGHT);
            Row.justifyContent(FlexAlign.End);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.actionMoreBuilder.bind(this)();
        Row.pop();
        Row.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=PickerComponent.js.map