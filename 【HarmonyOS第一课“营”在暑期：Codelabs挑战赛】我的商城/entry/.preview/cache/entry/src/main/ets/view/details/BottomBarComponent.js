/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AppFontSize, PERCENTAGE_100 } from '@bundle:com.example.list_harmony/entry/ets/common/constants/Constants';
import { BottomBar } from '@bundle:com.example.list_harmony/entry/ets/common/constants/DetailsConstants';
function __Text__setTextStyle() {
    Text.width(BottomBar.TEXT_WIDTH);
    Text.lineHeight(BottomBar.TEXT_LINE_HEIGHT);
    Text.textAlign(TextAlign.Center);
    Text.fontColor({ "id": 16777318, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
    Text.fontSize(AppFontSize.MIDDLE);
}
export default class BottomBarComponent extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.barHeight = undefined;
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.barHeight !== undefined) {
            this.barHeight = params.barHeight;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("view/details/BottomBarComponent.ets(32:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Divider.create();
            Divider.debugLine("view/details/BottomBarComponent.ets(33:7)");
            Divider.height(BottomBar.DIVIDER_HEIGHT);
            Divider.backgroundColor({ "id": 16777333, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            if (!isInitialRender) {
                Divider.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/BottomBarComponent.ets(36:7)");
            Row.padding({
                top: BottomBar.BAR_PADDING_TOP,
                bottom: BottomBar.BAR_PADDING_BOTTOM,
                right: BottomBar.BAR_PADDING_RIGHT
            });
            Row.width(PERCENTAGE_100);
            Row.height(this.barHeight);
            Row.backgroundColor({ "id": 16777320, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Row.justifyContent(FlexAlign.End);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/BottomBarComponent.ets(37:9)");
            Row.width(BottomBar.CONTAINER_SIZE_WIDTH);
            Row.height(BottomBar.CONTAINER_SIZE_HEIGHT);
            Row.backgroundImage({ "id": 0, "type": 30000, params: ['detail/detail_bottom_panel_background.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" }, ImageRepeat.NoRepeat);
            Row.backgroundImageSize(ImageSize.Cover);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 16777305, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Text.debugLine("view/details/BottomBarComponent.ets(38:11)");
            __Text__setTextStyle();
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 16777225, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Text.debugLine("view/details/BottomBarComponent.ets(40:11)");
            __Text__setTextStyle();
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        Row.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=BottomBarComponent.js.map