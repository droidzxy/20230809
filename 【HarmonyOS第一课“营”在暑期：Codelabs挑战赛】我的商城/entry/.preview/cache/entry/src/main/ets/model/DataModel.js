/*
 * The DataModel file instead of network data.
 */
export default class DataModel {
}
DataModel.TAB_VIEW_MENU = [
    { "id": 16777276, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
    { "id": 16777273, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
    { "id": 16777278, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
    { "id": 16777274, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
    { "id": 16777275, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
    { "id": 16777279, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
    { "id": 16777277, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
];
DataModel.BANNER = [
    {
        imgSrc: { "id": 0, "type": 30000, params: ['index/banner1.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" }
    },
    {
        imgSrc: { "id": 0, "type": 30000, params: ['index/banner2.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" }
    }
];
DataModel.HOME_MENU = [
    {
        menuName: { "id": 16777265, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        menuContent: { "id": 16777266, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        fontWeight: '500',
        fontColor: '#FFE92F4F'
    },
    {
        menuName: { "id": 16777271, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        menuContent: { "id": 16777272, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        fontWeight: '400',
        fontColor: '#FF000000'
    },
    {
        menuName: { "id": 16777269, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        menuContent: { "id": 16777270, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        fontWeight: '400',
        fontColor: '#FF000000',
    },
    {
        menuName: { "id": 16777267, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        menuContent: { "id": 16777268, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        fontWeight: '400',
        fontColor: '#FF000000'
    }
];
DataModel.GOOD_LIST = [
    {
        goodsName: { "id": 16777248, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        price: '1999',
        originalPrice: '2999',
        discounts: { "id": 16777250, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        label: { "id": 16777246, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        goodsImg: { "id": 0, "type": 30000, params: ['index/good1.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        goodsDescription: { "id": 16777249, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" }
    },
    {
        goodsName: { "id": 16777251, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        price: '1999',
        originalPrice: '',
        discounts: { "id": 16777253, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        label: { "id": 16777247, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        goodsImg: { "id": 0, "type": 30000, params: ['index/good2.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        goodsDescription: { "id": 16777252, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" }
    }
];
DataModel.TOOL_BAR = [
    {
        num: 0,
        text: { "id": 16777289, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        icon: { "id": 0, "type": 30000, params: ['index/home.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        icon_after: { "id": 0, "type": 30000, params: ['index/home_selected.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
    },
    {
        num: 1,
        text: { "id": 16777291, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        icon: { "id": 0, "type": 30000, params: ['index/news.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        icon_after: { "id": 0, "type": 30000, params: ['index/news_selected.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
    },
    {
        num: 2,
        text: { "id": 16777292, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        icon: { "id": 0, "type": 30000, params: ['index/shopping_cart.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        icon_after: { "id": 0, "type": 30000, params: ['index/shopping_cart_selected.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
    },
    {
        num: 3,
        text: { "id": 16777290, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        icon: { "id": 0, "type": 30000, params: ['index/mine.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        icon_after: { "id": 0, "type": 30000, params: ['index/mine_selected.png'], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
    }
];
DataModel.GOOD_SERVICE = [
    {
        id: 1,
        name: { "id": 16777261, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" },
        description: { "id": 16777260, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" }
    },
    {
        id: 2,
        name: null,
        description: { "id": 16777262, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" }
    },
    {
        id: 3,
        name: null,
        description: { "id": 16777263, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" }
    }
];
//# sourceMappingURL=DataModel.js.map