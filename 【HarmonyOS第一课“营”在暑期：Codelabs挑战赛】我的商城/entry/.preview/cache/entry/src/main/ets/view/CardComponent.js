/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CardStyle } from '@bundle:com.example.list_harmony/entry/ets/common/constants/Constants';
export default class CardComponent extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.children = undefined;
        this.paddingValue = undefined;
        this.marginValue = undefined;
        this.colorValue = undefined;
        this.radiusValue = undefined;
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.children !== undefined) {
            this.children = params.children;
        }
        if (params.paddingValue !== undefined) {
            this.paddingValue = params.paddingValue;
        }
        if (params.marginValue !== undefined) {
            this.marginValue = params.marginValue;
        }
        if (params.colorValue !== undefined) {
            this.colorValue = params.colorValue;
        }
        if (params.radiusValue !== undefined) {
            this.radiusValue = params.radiusValue;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            var _a, _b, _c, _d;
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("view/CardComponent.ets(30:5)");
            Column.padding((_a = this.paddingValue) !== null && _a !== void 0 ? _a : {
                top: CardStyle.CARD_PADDING_VERTICAL,
                bottom: CardStyle.CARD_PADDING_VERTICAL,
                left: CardStyle.CARD_PADDING_HORIZONTAL,
                right: CardStyle.CARD_PADDING_HORIZONTAL
            });
            Column.margin((_b = this.marginValue) !== null && _b !== void 0 ? _b : {
                top: CardStyle.CARD_MARGIN_TOP,
                left: CardStyle.CARD_MARGIN_HORIZONTAL,
                right: CardStyle.CARD_MARGIN_HORIZONTAL
            });
            Column.backgroundColor((_c = this.colorValue) !== null && _c !== void 0 ? _c : { "id": 16777319, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Column.borderRadius((_d = this.radiusValue) !== null && _d !== void 0 ? _d : CardStyle.CARD_RADIUS);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.children.bind(this)();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=CardComponent.js.map