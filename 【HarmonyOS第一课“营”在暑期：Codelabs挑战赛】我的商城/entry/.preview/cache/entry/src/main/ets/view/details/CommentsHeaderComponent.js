/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AppFontSize, AppFontWeight } from '@bundle:com.example.list_harmony/entry/ets/common/constants/Constants';
import { CommentHeader } from '@bundle:com.example.list_harmony/entry/ets/common/constants/DetailsConstants';
export default class CommentsHeaderComponent extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/CommentsHeaderComponent.ets(22:5)");
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 16777232, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Text.debugLine("view/details/CommentsHeaderComponent.ets(23:7)");
            Text.lineHeight(CommentHeader.LINE_HEIGHT);
            Text.textOverflow({ overflow: TextOverflow.Ellipsis });
            Text.textCase(TextCase.UpperCase);
            Text.fontSize(AppFontSize.MIDDLE);
            Text.fontColor({ "id": 16777336, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Text.fontWeight(AppFontWeight.BOLDER);
            Text.width(CommentHeader.LAYOUT_WEIGHT);
            Text.textAlign(TextAlign.Start);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("view/details/CommentsHeaderComponent.ets(32:7)");
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create();
            Text.debugLine("view/details/CommentsHeaderComponent.ets(33:9)");
            Text.width(CommentHeader.LAYOUT_WEIGHT);
            Text.textAlign(TextAlign.End);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Span.create({ "id": 16777227, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Span.debugLine("view/details/CommentsHeaderComponent.ets(34:11)");
            Span.textCase(TextCase.UpperCase);
            Span.fontSize(AppFontSize.SMALL);
            Span.fontColor(Color.Red);
            Span.fontWeight(AppFontWeight.BOLD);
            if (!isInitialRender) {
                Span.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Span.create({ "id": 16777233, "type": 10003, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Span.debugLine("view/details/CommentsHeaderComponent.ets(39:11)");
            Span.textCase(TextCase.UpperCase);
            Span.fontSize(AppFontSize.SMALL);
            Span.fontColor({ "id": 16777321, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Span.fontWeight(AppFontWeight.BOLD);
            if (!isInitialRender) {
                Span.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Span.create(CommentHeader.ARROW);
            Span.debugLine("view/details/CommentsHeaderComponent.ets(44:11)");
            Span.textCase(TextCase.UpperCase);
            Span.fontSize(AppFontSize.MIDDLE);
            Span.decoration({ type: TextDecorationType.None, color: Color.Red });
            Span.fontColor({ "id": 16777321, "type": 10001, params: [], "bundleName": "com.example.list_harmony", "moduleName": "entry" });
            Span.fontWeight(AppFontWeight.BOLD);
            if (!isInitialRender) {
                Span.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        Row.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
//# sourceMappingURL=CommentsHeaderComponent.js.map